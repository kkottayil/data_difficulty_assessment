
import matplotlib.pyplot as plt
from scipy import spatial
import numpy as np
import pandas as pd

def analyze_imbalance(data, target_column, minority_label = 1, verbose = 0):
    '''
    WIP implementation of one on the methods in [1] Napierala, K., & Stefanowski, J. (2016). 
    Types of minority class examples and their influence on learning classifiers from 
    imbalanced data. Journal of Intelligent Information Systems, 46(3), 563-597. The analysis 
    is limited to minority class and it considers every other class as 'majority class' in the
    analysis.
    
    :param data DataFrame: DataFrame with features and labels
    :param target_column str: label column column name 
    :param minority_label str: value of the minority label
    :param verbose bool: Flag to print debug info
    :returns analysis DataFrame: DataFrame that labels every input point as either 
                                 'majority','safe','borderline','rare', or 'outlier'.
                                 
    '''
    # Get features and labels
    X = data.drop(columns = [target_column])
    y = data[target_column].values
    
    # Check if minority label is indeed minority
    if data[target_column].value_counts().sort_values(ascending=False).index[0] == minority_label:
        print(f"WARNING: {minority_label} is the majority in labels.")
        print('-'*8)
        print('label','count')
        print('-'*8)
        print(data[target_column].value_counts())
        print('-'*8)
    points = X
    analysis = pd.DataFrame(len(X)*['majority_class'], columns=['analysis_label'])
    
    # Find 5 nearest neighbors; build tree
    tree = spatial.KDTree(points)
    minority_point_ids = np.where(y == minority_label)
    print(f"Search for {minority_label} in columns {target_column} lead to {len(minority_point_ids[0])} points") if verbose else None
    if len(minority_point_ids[0]) == 0:
        raise IOError(f"Dataframe column '{target_column}' has no value {minority_label}")
    
    # For each of the points in minority class, compute the label
    for point_id in list(minority_point_ids[0]):
        point_neighbor_ids = tree.query(points.values[point_id], k = 6)[1]
        labels_in_neighborhood = y[point_neighbor_ids]                
        # count the number of minority labels and compute ratio. 
        # TODO: Is a safe implementation with value_counts possible?
        # neighborhood_label_distribution = labels_in_neighborhood.value_counts().sort_index()
        # ratio = neighborhood_label_distribution[1]/neighborhood_label_distribution[0]
        num_same_label_points = sum(1 for i in labels_in_neighborhood if i == minority_label)-1 #Gives n+1 points including point itself
        num_other_label_points = sum(1 for i in labels_in_neighborhood if i != minority_label)
        print(f"Point {point_id}: {num_same_label_points}/{ num_other_label_points}")  if verbose else None
        if num_other_label_points == 0:
            analysis_label = 'safe'
        else:
            ratio = (num_same_label_points/num_other_label_points)
            if ratio == 1/4 :
                analysis_label = 'rare'
            elif ratio == 0:
                analysis_label = 'outlier'
            elif ratio == 2/3 or ratio == 3/2:
                analysis_label = 'borderline'    
            elif ratio == 4/1:
                analysis_label = 'safe'
        analysis.iloc[point_id] = analysis_label
    return analysis